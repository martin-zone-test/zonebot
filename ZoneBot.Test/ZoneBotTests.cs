using Microsoft.VisualStudio.TestTools.UnitTesting;
using ZoneBot.Logic;
using ZoneBot.Logic.Enums;

namespace ZoneBot.Test
{
    [TestClass]
    public class ZoneBotTests
    {
        [TestMethod]
        public void CanPlaceRobotOnBoard()
        {
            var robotnik = new Robot(new Table(5, 5));
            robotnik.Place(2, 3, Direction.NORTH);
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 2, YCord = 3, Facing = Direction.NORTH });
        }

        [TestMethod]
        public void CanMoveRobot()
        {
            var robotnik = new Robot(new Table(5, 5));
            robotnik.Place(2, 2, Direction.NORTH);
            robotnik.Move();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 2, YCord = 3, Facing = Direction.NORTH });

            robotnik.Place(2, 2, Direction.EAST);
            robotnik.Move();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 3, YCord = 2, Facing = Direction.EAST });

            robotnik.Place(2, 2, Direction.SOUTH);
            robotnik.Move();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 2, YCord = 1, Facing = Direction.SOUTH });

            robotnik.Place(2, 2, Direction.WEST);
            robotnik.Move();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 1, YCord = 2, Facing = Direction.WEST });
        }

        [TestMethod]
        public void CanTurnLeft()
        {
            var robotnik = new Robot(new Table(5, 5));
            robotnik.Place(0, 0, Direction.NORTH);
            robotnik.TurnLeft();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 0, YCord = 0, Facing = Direction.WEST });
            robotnik.TurnLeft();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 0, YCord = 0, Facing = Direction.SOUTH });
            robotnik.TurnLeft();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 0, YCord = 0, Facing = Direction.EAST });
            robotnik.TurnLeft();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 0, YCord = 0, Facing = Direction.NORTH });
        }

        [TestMethod]
        public void CanTurnRight()
        {
            var robotnik = new Robot(new Table(5, 5));
            robotnik.Place(0, 0, Direction.NORTH);
            robotnik.TurnRight();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 0, YCord = 0, Facing = Direction.EAST });
            robotnik.TurnRight();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 0, YCord = 0, Facing = Direction.SOUTH });
            robotnik.TurnRight();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 0, YCord = 0, Facing = Direction.WEST });
            robotnik.TurnRight();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 0, YCord = 0, Facing = Direction.NORTH });
        }

        [TestMethod]
        public void PlaceActionThatPutsRobotOffTableIsIgnored()
        {
            var robotnik = new Robot(new Table(5, 5));
            robotnik.Place(0, 5, Direction.WEST);
            Assert.IsNull(robotnik.Location);
        }

        [TestMethod]
        public void MoveThatPutsRobotOffTableIsIgnored()
        {
            var robotnik = new Robot(new Table(5, 5));
            robotnik.Place(4, 4, Direction.EAST);
            robotnik.Move();
            AssertLocationsAreEqual(robotnik.Location, new Location() { XCord = 4, YCord = 4, Facing = Direction.EAST });
        }

        [TestMethod]
        public void AllActionsPriorToPlaceActionAreIgnored()
        {
            var robotnik = new Robot(new Table(5, 5));
            robotnik.TurnLeft();
            Assert.IsNull(robotnik.Location);
            Assert.IsNull(robotnik.Report());

            robotnik.TurnRight();
            Assert.IsNull(robotnik.Location);
            Assert.IsNull(robotnik.Report());

            robotnik.Move();
            Assert.IsNull(robotnik.Location);
            Assert.IsNull(robotnik.Report());
        }

        [TestMethod]
        public void CanReportOnActions()
        {
            var robotnik = new Robot(new Table(5, 5));
            robotnik.Place(0,0,Direction.NORTH);
            robotnik.Move();

            Assert.AreEqual(robotnik.Report(), "0,1,NORTH");
        }

        [TestMethod]
        public void CanReadPlaceInput()
        {
            Location location = RobotController.ReadPlaceInput("PLACE 0,1,NORTH");
            Assert.AreEqual(location, new Location { XCord = 0, YCord = 1, Facing = Direction.NORTH });
        }

        public void AssertLocationsAreEqual(Location locationA, Location locationB)
        {
            Assert.AreEqual(locationA.XCord, locationB.XCord);
            Assert.AreEqual(locationA.YCord, locationB.YCord);
            Assert.AreEqual(locationA.Facing, locationB.Facing);
        }
    }
}
