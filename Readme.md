# ZoneBot
ZoneBot has been built as both a portable dot net core 2.0 console app and dot net core 2.0 Web API / JS App (Web App).

The Web App version can be run using Visual Studio or by visting the hosted version here: http://zone-bot.azurewebsites.net/.

A pre-compiled version of the console app can be found in: PublishedOutput.zip. It can be extracted and run from the command line with 'dotnet ZoneBot.dll'.


Example input:

- PLACE 0,0,NORTH|MOVE|REPORT

- PLACE 0,0,NORTH|LEFT|REPORT

- PLACE 1,2,EAST|MOVE|MOVE|LEFT|MOVE|REPORT

- PLACE 4,4,NORTH|MOVE|REPORT

## Testing

### Test Case 1
ZoneBot cannot move until it has been placed.

Input: MOVE|LEFT|REPORT

Expected output: Beep boop boop beepa deep boop. (Pssst, you've got to PLACE me first!).

Actual output: Beep boop boop beepa deep boop. (Pssst, you've got to PLACE me first!).

Outcome: Pass

### Test Case 2
ZoneBot can be placed on the table.

Input: PLACE 0,0,NORTH|REPORT

Expected output: 0,0,NORTH

Actual output: 0,0,NORTH

Outcome: PASS

### Test Case 3
ZoneBot can report its location.

Input: PLACE 0,1,NORTH|REPORT

Expected output: 0,1,NORTH

Actual output: PASS

### Test Case 4
ZoneBot can move forward once placed.

Input: PLACE 0,0,NORTH|MOVE|REPORT

Expected output: 0,1,NORTH

Actual output: 0,1,NORTH

Actual output: PASS

### Test Case 5
ZoneBot can turn left and right.

Input: PLACE 0,0,NORTH|LEFT|REPORT|RIGHT|REPORT

Expected output:    0,0,WEST    0,0,NORTH

Actual output:      0,0,WEST    0,0,NORTH

Outcome: PASS

### Test Case 6
ZoneBot will ignore moves that would cause it to fall from the table.

Input: PLACE 0,0,WEST|MOVE|REPORT

Expected output: 0,0,WEST

Actual output: 0,0,WEST

Outcome: PASS

### Test Case 7
ZoneBot will ignore place actions that are not on the table

Input: 	RESET (run first to clear the table)
	PLACE 5,0,WEST|REPORT

Expected output: Beep boop boop beepa deep boop. (Pssst, you've got to PLACE me first!).

Actual output: Beep boop boop beepa deep boop. (Pssst, you've got to PLACE me first!).

Outcome: PASS