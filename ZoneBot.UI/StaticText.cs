﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZoneBot.ConsoleApp
{
    public static class StaticText
    {
        public static string WelcomeMessage =
            @"...
                  ,--.    ,--.
                 ((O ))--((O ))
               ,'_`--'____`--'_`.
              _:  ____________  :_
             | | ||::::::::::|| | |		Hi, I'm ZoneBot.
             | | ||::::::::::|| | |		You can control me with the following commands:
             | | ||::::::::::|| | |		PLACE [0-4],[0-4],[NORTH/SOUTH/EAST/WEST]
             |_| |/__________\| |_|		MOVE
               |________________|		LEFT
            __..-'            `-..__		RIGHT
         .-| : .----------------. : |-.		REPORT
       ,\ || | |\______________/| | || /.       RESET
      /`.\:| | ||  __  __  __  || | |;/,'\      EXIT
     :`-._\;.| || '--''--''--' || |,:/_.-':     
     |    :  | || .----------. || |  :    |     Multiple action commands can be pipe seperated, like so:
     |    |  | || '-Zone-Bot-' || |  |    |     PLACE 0,0,NORTH|MOVE|RIGHT|MOVE...
     |    |  | ||   _   _   _  || |  |    |
     :,--.;  | ||  (_) (_) (_) || |  :,--.;
     (`-'|)  | ||______________|| |  (|`-')
      `--'   | |/______________\| |   `--'
             |____________________|
              `.________________,'
               (_______)(_______)
               (_______)(_______)
               (_______)(_______)
               (_______)(_______)
              |        ||        |
              '--------''--------'
...";
    }
}
