﻿using System;
using System.IO;
using System.Text;
using ZoneBot.Logic;

namespace ZoneBot.ConsoleApp
{
    class Program
    {
        const int tableWidth = 5;
        const int tableHeight = 5;

        static void Main(string[] args)
        {
            Console.SetWindowSize(Console.LargestWindowWidth < 110 ? Console.LargestWindowWidth : 110, Console.LargestWindowHeight < 35 ? Console.LargestWindowHeight : 35);
            Console.OutputEncoding = Encoding.UTF8;
            Console.WriteLine(StaticText.WelcomeMessage);

            Robot zoneBot = new Robot(new Table(tableWidth, tableHeight));
            RobotController zoneBotController = new RobotController(zoneBot);

            string input;
            while ((input = Console.ReadLine()) != "EXIT")
            {
                if(input == "RESET")
                {
                    zoneBot = new Robot(new Table(tableWidth, tableHeight));
                    zoneBotController = new RobotController(zoneBot);
                    continue;
                }
                foreach (string inputCommand in input.Split('|'))
                {
                    string output = zoneBotController.ProcessInput(inputCommand);
                    if (!string.IsNullOrEmpty(output))
                    {
                        Console.WriteLine(output);
                    }
                }
            }
        }
    }
}
