﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZoneBot.Web.Models
{
    public class ResponseModel
    {
        public string Output { get; set; }

        public string Location { get; set; }
    }
}
