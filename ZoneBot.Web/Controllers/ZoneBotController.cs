﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZoneBot.Web.Models;
using ZoneBot.Logic;

namespace ZoneBot.Web.Controllers
{
    [Produces("application/json")]
    [Route("api/zonebot")]
    public class ZoneBotController : Controller
    {
        [HttpGet]
        public ResponseModel GetResult(string command, string location, int width, int height)
        {
            Robot zoneBot = new Robot(new Table(width, height));
            if (!string.IsNullOrEmpty(location))
            {
                zoneBot.Location = RobotController.ReadPlaceInput(location);
            }
            RobotController zoneBotController = new RobotController(zoneBot);

            string result = string.Empty;
            foreach (string inputCommand in command.Split('|'))
            {
                string output = zoneBotController.ProcessInput(inputCommand.ToUpper());
                if (!string.IsNullOrEmpty(output))
                {
                    result += output + "\n";
                }
            }
            return new ResponseModel { Output = result, Location = zoneBot.Report() };
        }
    }
}