jQuery(function ($, undefined) {
    var term = $('#term').terminal(function (command) {
        if (command !== '') {
            if (command === 'RESET') {
                $('#location').val('');
                return;
            }
            var location = $('#location').val();
            fetch("/api/zonebot/?command=" + command + "&location=" + location + "&width=5&height=5")
                .then((resp) => resp.json())
                .then(function (data) {
                    term.echo(data.output);
                    $('#location').val(data.location);
                });
        }
    }, {
            greetings: welcomeMessage,
            name: 'Zone_Bot',
            prompt: 'bot> '
        });
});