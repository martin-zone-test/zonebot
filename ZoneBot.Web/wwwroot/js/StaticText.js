﻿// This is a bit rough way of storing the text. However, it was mine field trying
// to get the intro from an endpoint using the terminal plugin though.
var welcomeMessage = "" +
    "\n                  ,--.    ,--." +
    "\n                 ((O ))--((O ))" +
    "\n               ,'_`--'____`--'_`." +
    "\n              _:  ____________  :_" +
    "\n             | | ||::::::::::|| | |		    Hi, I'm ZoneBot." +
    "\n             | | ||::::::::::|| | |		    You can control me with the following commands:" +
    "\n             | | ||::::::::::|| | |		    PLACE [0-4],[0-4],[NORTH/SOUTH/EAST/WEST]" +
    "\n             |_| |/__________\\| |_|		    MOVE" +
    "\n               |________________|		      LEFT" +
    "\n            __..-'            `-..__	       RIGHT" +
    "\n         .-| : .----------------. : |-.	    REPORT" +
    "\n       ,\\ || | |\\______________/| | || /.      RESET" +
    "\n      /`.\\:| | ||  __  __  __  || | |;/,'\      " +
    "\n     :`-._\\;.| || '--''--''--' || |,:/_.-':     " +
    "\n     |    :  | || .----------. || |  :    |    Multiple action commands can be pipe seperated, like so:" +
    "\n     |    |  | || '-Zone-Bot-' || |  |    |    PLACE 0,0,NORTH|MOVE|RIGHT|MOVE..." +
    "\n     |    |  | ||   _   _   _  || |  |    |" +
    "\n     :,--.;  | ||  (_) (_) (_) || |  :,--.;" +
    "\n     (`-'|)  | ||______________|| |  (|`-')" +
    "\n      `--'   | |/______________\\| |   `--'" +
    "\n             |____________________|" +
    "\n              `.________________,'" +
    "\n               (_______)(_______)" +
    "\n               (_______)(_______)" +
    "\n               (_______)(_______)" +
    "\n               (_______)(_______)" +
    "\n              |        ||        |" +
    "\n              '--------''--------'" +
    "\n...";
