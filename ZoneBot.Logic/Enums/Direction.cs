﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZoneBot.Logic.Enums
{
    public enum Direction
    {
        NORTH,
        SOUTH,
        EAST,
        WEST
    }
}
