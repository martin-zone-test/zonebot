﻿using System;
using System.Collections.Generic;
using System.Text;
using ZoneBot.Logic.Enums;

namespace ZoneBot.Logic
{
    public class RobotController
    {
        private readonly Robot _zoneBot;

        public RobotController(Robot zoneBot)
        {
            _zoneBot = zoneBot;
        }

        /// <summary>
        /// Interpret input command and apply action to robot.
        /// </summary>
        /// <param name="command"></param>
        /// <returns>Response from ZoneBot</returns>
        public string ProcessInput(string command)
        {
            StringBuilder output = new StringBuilder();
            if (command.ToUpper().StartsWith("PLACE"))
            {
                Location startPoint = ReadPlaceInput(command);
                if (startPoint == null)
                {
                    output.Append("Ooops! I did't understand that, please try again.");
                }
                else
                {
                    _zoneBot.Place(startPoint.XCord, startPoint.YCord, startPoint.Facing);
                }
                return output.ToString();
            }

            switch (command.ToUpper())
            {
                case "MOVE":
                    _zoneBot.Move();
                    break;
                case "LEFT":
                    _zoneBot.TurnLeft();
                    break;
                case "RIGHT":
                    _zoneBot.TurnRight();
                    break;
                case "REPORT":
                    string result = _zoneBot.Report();
                    if(result != null)
                    {
                        output.Append(result);
                        output.Append(RenderCurrentTable(_zoneBot.Location));
                    }
                    else
                    {
                        output.Append("Beep boop boop beepa deep boop. (Pssst, you've got to PLACE me first!)");
                    }
                    break;
                default:
                    output.Append("Ooops! I did't understand that, please try again.");
                    break;
            }

            return output.ToString();
        }

        /// <summary>
        /// Print current table to string
        /// </summary>
        /// <param name="location">Robot's current location</param>
        private string RenderCurrentTable(Location location)
        {
            string[,] table = new string[_zoneBot.Table.Width, _zoneBot.Table.Length];
            StringBuilder output = new StringBuilder();
            output.Append(Environment.NewLine);
            for (int row = 0; row < _zoneBot.Table.Length; row++)
            {
                for (int column = 0; column < _zoneBot.Table.Width; column++)
                {
                    if (location.XCord == column && ((location.YCord * -1) + (_zoneBot.Table.Length - 1)) == row)
                    {
                        switch (location.Facing)
                        {
                            case Direction.NORTH:
                                output.Append("↑ ");
                                break;
                            case Direction.EAST:
                                output.Append("→ ");
                                break;
                            case Direction.SOUTH:
                                output.Append("↓ ");
                                break;
                            case Direction.WEST:
                                output.Append("← ");
                                break;
                        }
                    }
                    else
                    {
                        output.Append("O ");
                    }
                }
                output.Append(Environment.NewLine);
            }
            return output.ToString();
        }

        /// <summary>
        /// Convert input string to location.
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns>Location to place bot or null if interperated</returns>
        public static Location ReadPlaceInput(string inputString)
        {
            string[] locationParams = inputString.ToUpper().Replace("PLACE ", "").Split(',');
            if (locationParams.Length == 3)
            {
                if (int.TryParse(locationParams[0], out int xCord) &&
                    int.TryParse(locationParams[1], out int yCord) &&
                    Enum.TryParse(locationParams[2], out Direction direction))
                {
                    return new Location() { XCord = xCord, YCord = yCord, Facing = direction };
                }
            }
            return null;
        }
    }
}
