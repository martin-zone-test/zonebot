﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZoneBot.Logic
{
    public class Table
    {
        public readonly int Width;

        public readonly int Length;

        public Table(int width, int length)
        {
            Width = width;
            Length = length;
        }
    }
}
