﻿using System;
using System.Collections.Generic;
using System.Text;
using ZoneBot.Logic.Enums;

namespace ZoneBot.Logic
{
    public class Location
    {
        public int XCord { get; set; }

        public int YCord { get; set; }

        public Direction Facing { get; set; }

        public override bool Equals(object other)
        {
            var compareTo = other as Location;
            if (compareTo == null)
                return false;
            return XCord == compareTo.XCord &&
                YCord == compareTo.YCord &&
                Facing == compareTo.Facing;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
