﻿using System;
using System.Collections.Generic;
using System.Text;
using ZoneBot.Logic.Enums;

namespace ZoneBot.Logic
{
    public class Robot
    {
        public Location Location { get; set; }

        public readonly Table Table;

        public Robot(Table table)
        {
            Table = table;
        }

        public void Place(int xCord, int yCord, Direction facing)
        {
            if(xCord >= 0 && xCord < (Table.Width) && yCord >= 0 && yCord < (Table.Length))
            {
                Location = new Location { Facing = facing, XCord = xCord, YCord = yCord };
            }
        }

        public void Move()
        {
            if (Location != null)
            {
                switch (Location.Facing)
                {
                    case Direction.NORTH:
                        if (Location.YCord < Table.Length - 1)
                        {
                            Location.YCord++;
                        }
                        break;
                    case Direction.EAST:
                        if (Location.XCord < Table.Width - 1)
                        {
                            Location.XCord++;
                        }
                        break;
                    case Direction.SOUTH:
                        if (Location.YCord > 0)
                        {
                            Location.YCord--;
                        }
                        break;
                    case Direction.WEST:
                        if (Location.XCord > 0)
                        {
                            Location.XCord--;
                        }
                        break;
                }
            }
        }

        public void TurnLeft()
        {
            if(Location != null)
            {
                switch (Location.Facing)
                {
                    case Direction.NORTH:
                        Location.Facing = Direction.WEST;
                        break;
                    case Direction.EAST:
                        Location.Facing = Direction.NORTH;
                        break;
                    case Direction.SOUTH:
                        Location.Facing = Direction.EAST;
                        break;
                    case Direction.WEST:
                        Location.Facing = Direction.SOUTH;
                        break;
                }
            }
        }

        public void TurnRight()
        {
            if (Location != null)
            {
                switch (Location.Facing)
                {
                    case Direction.NORTH:
                        Location.Facing = Direction.EAST;
                        break;
                    case Direction.EAST:
                        Location.Facing = Direction.SOUTH;
                        break;
                    case Direction.SOUTH:
                        Location.Facing = Direction.WEST;
                        break;
                    case Direction.WEST:
                        Location.Facing = Direction.NORTH;
                        break;
                }
            }
        }

        public string Report()
        {
            if(Location != null)
            {
                return $"{Location.XCord},{Location.YCord},{Location.Facing}";
            }
            return null;
        }
    }
}